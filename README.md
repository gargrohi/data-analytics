# Modeling of Web ecommerce Statistics and Data Analytics  #

The report contains general aspects and insights from the data of sales and traffic.

### What is this repository for? ###

* Analyzed user behavior statistics of ecommerce data with respect to conversion rate, bounce rate, add to cart rate etc for different
platforms using statistical techniques by programming in R Language .
* Performed Kernel Density Estimation, Multivariate Linear Regression, Kernel Regression to model the relationship between explanatory
variables and response variable.
* Analysis revolves around conversion rate, bounce rate and add to cart rate and their corresponding correlation coefficient  with confidence 
intervals.
* Version: 1.0
* URL: http://bit.ly/dataAnalytics


### How do I get set up? ###

* Install open source R statistical language tool 3.1.

### Contribution ###

* Zappos Dataset 

### Who do I talk to? ###

* Prof Sathyanaraya Raghavachary
* Zappos data team